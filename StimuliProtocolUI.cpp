#include "StimuliProtocolUI.h"

StimuliProtocolUI::StimuliProtocolUI() {
    createNumberOfStimuliInput();
    createStimulusTimeInput();
    createTimeBetweenStimuliInput();
}

StimuliProtocolUI::~StimuliProtocolUI() {}

int StimuliProtocolUI::getNumberStimuliSelected() {
    return getNumberStimuli() -> value();
}

int StimuliProtocolUI::getStimuliTimeSelected() {
    return getStimuliTime() -> value();
}

int StimuliProtocolUI::getTimeBetweenStimuliSelected() {
    return getTimeBetweenStimuli() -> value();
}

void StimuliProtocolUI::createNumberOfStimuliInput() {
    QHBoxLayout* layout = new QHBoxLayout();
    QLabel* title = new QLabel("Number of stimuli:");
    layout -> addWidget(title);

    setNumberStimuli(new QSpinBox());
    getNumberStimuli() -> setRange(0, 20);
    getNumberStimuli() -> setSingleStep(1);
    getNumberStimuli() -> setValue(0);
    layout -> addWidget(getNumberStimuli());

    addLayout(layout);
}

void StimuliProtocolUI::createStimulusTimeInput() {
    QHBoxLayout* layout = new QHBoxLayout();
    QLabel* title = new QLabel("Stimulus time (sec):");
    layout -> addWidget(title);

    setStimuliTime(new QSpinBox());
    getStimuliTime() -> setRange(0, 20);
    getStimuliTime() -> setSingleStep(1);
    getStimuliTime() -> setValue(0);
    layout -> addWidget(getStimuliTime());

    addLayout(layout);
}

void StimuliProtocolUI::createTimeBetweenStimuliInput() {
    QHBoxLayout* layout = new QHBoxLayout();
    QLabel* title = new QLabel("Time between stimuli (sec):");
    layout -> addWidget(title);

    setTimeBetweenStimuli(new QSpinBox());
    getTimeBetweenStimuli() -> setRange(0, 20);
    getTimeBetweenStimuli() -> setSingleStep(1);
    getTimeBetweenStimuli() -> setValue(0);
    layout -> addWidget(getTimeBetweenStimuli());

    addLayout(layout);
}

QSpinBox* StimuliProtocolUI::getNumberStimuli() const {
    return numberStimuli;
}

void StimuliProtocolUI::setNumberStimuli(QSpinBox* value) {
    numberStimuli = value;
}

QSpinBox* StimuliProtocolUI::getStimuliTime() const {
    return stimuliTime;
}

void StimuliProtocolUI::setStimuliTime(QSpinBox* value) {
    stimuliTime = value;
}

QSpinBox* StimuliProtocolUI::getTimeBetweenStimuli() const {
    return timeBetweenStimuli;
}

void StimuliProtocolUI::setTimeBetweenStimuli(QSpinBox* value) {
    timeBetweenStimuli = value;
}

