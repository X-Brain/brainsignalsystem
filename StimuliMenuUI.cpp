#include "StimuliMenuUI.h"

StimuliMenuUI::StimuliMenuUI(QWidget *parent) : QWidget(parent) {
    generalDataUI = new GeneralDataUI;
    protocolsUI = new ProtocolsUI();
    dataLogUI = new DataLogUI();
    controlPanelStimuliScreenUI = new ControlPanelStimuliScreenUI(protocolsUI, dataLogUI);

    scrollDataLog = new QScrollArea();

    createContent();
    setStyleSheet("background-color: white;");

    setWindowTitle(tr("X-Brain EEG"));
}

StimuliMenuUI::~StimuliMenuUI() {}

void StimuliMenuUI::createContent() {
    QVBoxLayout* contentBox = new QVBoxLayout();

    contentBox -> addWidget(getGeneralDataUI());
    contentBox -> addWidget(getProtocolsUI());
    contentBox -> addLayout(getControlPanelStimuliScreenUI());

    getScrollDataLog() -> setWidget(getDataLogUI());
    contentBox -> addWidget(getScrollDataLog());

    setLayout(contentBox);
}

void StimuliMenuUI::closeEvent(QCloseEvent *event) {
    getControlPanelStimuliScreenUI() -> closeScreen();

    event -> accept();
}

GeneralDataUI* StimuliMenuUI::getGeneralDataUI() const {
    return generalDataUI;
}

void StimuliMenuUI::setGeneralDataUI(GeneralDataUI* value) {
    generalDataUI = value;
}

ProtocolsUI* StimuliMenuUI::getProtocolsUI() const {
    return protocolsUI;
}

void StimuliMenuUI::setProtocolsUI(ProtocolsUI* value) {
    protocolsUI = value;
}

ControlPanelStimuliScreenUI* StimuliMenuUI::getControlPanelStimuliScreenUI() const {
    return controlPanelStimuliScreenUI;
}

void StimuliMenuUI::setControlPanelStimuliScreenUI(ControlPanelStimuliScreenUI* value) {
    controlPanelStimuliScreenUI = value;
}

DataLogUI* StimuliMenuUI::getDataLogUI() const {
    return dataLogUI;
}

void StimuliMenuUI::setDataLogUI(DataLogUI* value) {
    dataLogUI = value;
}

QScrollArea* StimuliMenuUI::getScrollDataLog() const {
    return scrollDataLog;
}

void StimuliMenuUI::setScrollDataLog(QScrollArea* value) {
    scrollDataLog = value;
}
