#include "StimuliRendering.h"

StimuliRendering::StimuliRendering(DataLogUI* dataLogUI, QThread *parent) : QThread(parent) {
    this -> stimuliScreen = new StimuliScreen();
    this -> dataLogUI = dataLogUI;

    timer = nullptr;
    isPaused = true;
    initTime = 0;
}

StimuliRendering::~StimuliRendering() {}

void StimuliRendering::play() {
    setIsPaused(false);
}

void StimuliRendering::pause() {
    setIsPaused(true);
    setInitTime(getTimer() -> elapsed());
}

void StimuliRendering::restart() {
    getStimulusScreen() -> clear();
    clearShowed();
    setInitTime(0);

    if(getTimer() != nullptr) {
        getTimer() -> restart();

    } else {
        setTimer(new QTime());
    }
}

void StimuliRendering::closeStimuliScreen() {
    getStimulusScreen() -> close();
    terminate();
    quit();
}

void StimuliRendering::run() {
    while(true) {
        if(!(getIsPaused())) {
            delete getTimer();
            setTimer(nullptr);
            setTimer(new QTime(0, 0, 0, getInitTime()));

            runStimuli();

            setIsPaused(true);
        }
    }
}

void StimuliRendering::runStimuli() {
    int indexInUse = 0;
    getTimer() -> start();

    while(canRun() && !(getIsPaused())) {
        for(int i = 0; i < getProtocol() -> getNumberOfStimuli(); i++) {

            if(getProtocol() -> getStimulus(i) -> getActionTime() > getTimer() -> elapsed()) {
                break;

            } else {
                if((getProtocol() -> getStimulus(i) -> getActionTime() <= getTimer() -> elapsed()) &&
                        ((getProtocol() -> getStimulus(i) -> getActionTime() + getProtocol() -> getStimulus(i) -> getDurationTime()) >= getTimer() -> elapsed())) {

                    indexInUse = i;
                    getProtocol() -> getStimulus(i) -> run(getDataLogUI());

                    break;
                } else {
                    if((indexInUse == i) && (getProtocol() -> getStimulus(i) -> getIsShowed()) &&
                            (getProtocol() -> getStimulus(i) -> getActionTime() + getProtocol() -> getStimulus(i) -> getDurationTime()) < getTimer() -> elapsed()) {
                        getProtocol() -> getStimulus(i) -> stop(getDataLogUI());
                    }
                }
            }
        }
    }
}

void StimuliRendering::clearShowed() {
    for(int i = 0; i < getProtocol() -> getNumberOfStimuli(); i++) {
        getProtocol() -> getStimulus(i) -> setIsShowed(false);
        getProtocol() -> getStimulus(i) -> setIsStopped(false);
    }
}

bool StimuliRendering::canRun() {
    for(int i = 0; i < getProtocol() -> getNumberOfStimuli(); i++) {
        if(!(getProtocol() -> getStimulus(i) -> getIsStopped())) {
            return true;
        }
    }

    restart();

    return false;
}

StimuliScreen* StimuliRendering::getStimulusScreen() const {
    return stimuliScreen;
}

void StimuliRendering::setStimulusScreen(StimuliScreen* value) {
    stimuliScreen = value;
}

Protocol* StimuliRendering::getProtocol() const {
    return protocol;
}

void StimuliRendering::setProtocol(Protocol* value) {
    protocol = value;
}

QTime* StimuliRendering::getTimer() const {
    return timer;
}

void StimuliRendering::setTimer(QTime* value) {
    timer = value;
}

bool StimuliRendering::getIsPaused() const {
    return isPaused;
}

void StimuliRendering::setIsPaused(bool value) {
    isPaused = value;
}

int StimuliRendering::getInitTime() const {
    return initTime;
}

void StimuliRendering::setInitTime(int value) {
    initTime = value;
}

DataLogUI* StimuliRendering::getDataLogUI() const {
    return dataLogUI;
}

void StimuliRendering::setDataLogUI(DataLogUI* value) {
    dataLogUI = value;
}


