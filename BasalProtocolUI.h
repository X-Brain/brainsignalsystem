#ifndef BASALPROTOCOL_UI_H
#define BASALPROTOCOL_UI_H

#include <QWidget>
#include <ProtocolUI.h>

class BasalProtocolUI : public ProtocolUI {

    public:
        BasalProtocolUI();
        ~BasalProtocolUI();
};

#endif // BASALPROTOCOL_UI_H
